#!/bin/bash
set -e
echo "export CONTAINER_ID=`head -1 /proc/self/cgroup|cut -d/ -f3`" >> /root/.bashrc
source ~/ros_catkin_ws/install_isolated/setup.bash
sleep ${START_DELAY_WAIT:-0}
sed -i -e 's/log4j.logger.ros=.*$/log4j.logger.ros='"${LOGGING_LEVEL:-WARN}"'/g' $ROS_ROOT/config/rosconsole.config
EXECUTBLE="./devel/lib/koralROS/koralNode "
if [ "$DEBUG" == "true" ]; then
    if ! test -f /usr/bin/valgrind; then
        apt update && apt install -y valgrind
    fi
    EXECUTBLE="valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file=valgrind-out.txt ./devel/lib/koralROS/koralNode "
fi
$EXECUTBLE --width=${WIDTH:-1280} --height=${HEIGHT:-720} --maxFeatureCount=${MAX_FEATURE_COUNT:-132500} --fastThreshold=${FAST_THRESHOLD:-40} --matchThreshold=${MATCH_THRESHOLD:-25} --scaleLevels=${SCALE_LEVELS:-8} --scaleFactor=${SCALE_FACTOR:-1.2} --topicName=${TOPIC_NAME:-image} --containerId=${CONTAINER_ID:-1} --thread_count=${THREAD_COUNT:-3} --postgresHostIP=${POSTGRES_HOST_IP:-} --postgresPort=${POSTGRES_PORT:-} --postgresUser=${POSTGRES_USER:-postgres} --postgresPassword=${POSTGRES_PASSWORD:-password} --postgresDBName=${POSTGRES_DB_NAME:-postgres} --displayFeatures=${DISPLAY_FEATURES:-false} --morton_code_only=${MORTON_CODE_ONLY:-false} --cameraTransport=${CAMERA_TRANSPORT:-compressed} --color_encoding=${COLOR_ENCODING:-BGR8} --target_table_name=${TARGET_TABLE_NAME:-m0} --postgresSchemas=${POSTGRES_SCHEMAS:-cognition} --target_features_count=${TARGET_FEATURES_COUNT:-10000} --process_changed_pixels_only=${PROCESS_CHANGED_PIXELS_ONLY:-false} --bgsubtractor_history_Length=${BGSUBTRACTOR_HISTORY_LENGTH:-500} --bgsubtractor_var_threshold=${BGSUBTRACTOR_VAR_THRESHOLD:-16} --bgsubtractor_detect_shadows=${BGSUBTRACTOR_DETECT_SHADOWS:-false} --iterations_limit=${ITERATIONS_LIMIT:-0} --error_count_limit=${ERROR_COUNT_LIMIT:--1}
