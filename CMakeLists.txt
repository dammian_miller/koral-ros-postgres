cmake_minimum_required(VERSION 2.8.3)
project(koralROS)
file(GLOB HEADERS "include/*.hpp")
file(GLOB HEADERS "include/*.h")
file(GLOB SOURCES  "src/*.cpp")
FIND_PACKAGE(CUDA REQUIRED)
FIND_PACKAGE(OpenCV REQUIRED)
find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  roscpp
  std_msgs
  image_transport
)
# -gencode arch=compute_75,code=sm_75
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O3 -std=c++14 -march=native -funroll-all-loops -fpeel-loops -ftracer -ftree-vectorize -mavx2 -mfma -fomit-frame-pointer -mbmi2")
SET(CUDA_NVCC_FLAGS "-D_MWAITXINTRIN_H_INCLUDED -lineinfo -Xptxas -v -Xcompiler -fopenmp -use_fast_math -O3 -gencode=arch=compute_52,code=sm_52 -gencode=arch=compute_60,code=sm_60 -gencode=arch=compute_61,code=sm_61 -gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_75,code=sm_75 -gencode=arch=compute_80,code=sm_80 -gencode=arch=compute_80,code=compute_80 --default-stream per-thread " CACHE STRING "nvcc flags" FORCE)
SET (CUDA_VERBOSE_BUILD ON CACHE BOOL "nvcc verbose" FORCE)
SET(LIB_TYPE STATIC) 
include_directories(include)
CUDA_ADD_LIBRARY(koral ${LIB_TYPE} src/CUDALERP.cu src/CLATCH.cu src/CUDAK2NN.cu)

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES koralROS libpq-pool
  CATKIN_DEPENDS cv_bridge roscpp std_msgs image_transport
#  DEPENDS system_lib
)

include_directories(
# include
  ${catkin_INCLUDE_DIRS}
)

add_executable(koralNode src/koral_node.cpp)

add_dependencies(koralNode koral)

find_package(cityhash REQUIRED)

# add pqxx per: https://stackoverflow.com/questions/57810153/including-libpqxx-results-in-failed-build-on-wsl-using-cmake
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lpqxx -lpq -I/usr/include/postgresql -I/opt/vcpkg/installed/x64-linux/include -DPQXX_HIDE_EXP_OPTIONAL -std=c++14 -march=native -mtune=native -mbmi2 -O3")
set(PQXX /usr/local/include/pqxx)

find_library(PQXX_LIB pqxx)
find_library(PQ_LIB pq)

find_package(morton-nd CONFIG REQUIRED)

target_link_libraries(koralNode PRIVATE
   ${catkin_LIBRARIES} ${PQXX_LIB} ${PQ_LIB} ${CITYHASH_LIBRARIES} morton-nd::MortonND
  koral
  ${OpenCV_LIBS}
 )
