#include "koralROS/Detector.h"
#include "koralROS/Matcher.h"
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <getopt.h>

#include <sstream>
#include <iostream>
#include <thread>    
#include <vector>
#include <pqxx/pqxx>
#include <libpq-fe.h>
#include <netinet/in.h>

#include <city.h>

#include <limits>
#include <inttypes.h>

#include <boost/thread/thread.hpp>
#include "libpq-pool/pgbackend.h"

#include <morton-nd/mortonND_BMI2.h>
#include <morton-nd/mortonND_LUT.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include <unistd.h>
#include <opencv2/cudabgsegm.hpp>
// #include <opencv2/cudalegacy.hpp>

using MortonND_8D = mortonnd::MortonNDBmi<8, uint64_t>;

// https://stackoverflow.com/questions/12219928/pack-unpack-short-into-int
int32_t Pack16_to_32(int16_t a, int16_t b)
{
	return (int32_t)( (uint32_t)a<<16 | (uint32_t)b );
}

int16_t UnpackA(int32_t x)
{
   return (int16_t)(((uint32_t)x)>>16);
}

int16_t UnpackB(int32_t x)
{
   return (int16_t)(((uint32_t)x)&0xffff);
}


// https://codereview.stackexchange.com/questions/80386/packing-and-unpacking-two-32-bit-integers-into-an-unsigned-64-bit-integer
uint64_t combine(uint32_t low, uint32_t high)
{
     return (((uint64_t) high) << 32) | ((uint64_t) low);
}

uint32_t high(uint64_t combined)
{
    return combined >> 32;
}

uint32_t low(uint64_t combined)
{
    uint64_t mask = std::numeric_limits<uint32_t>::max();
    return mask & combined; // should I just do "return combined;" which gives same result?
}

// https://stackoverflow.com/questions/22648978/c-how-to-find-the-length-of-an-integer
int int_length(int i) {
    int l=0;
    for(;i;i/=10) l++;
    return l==0 ? 1 : l;
}

int int64_length(int64_t i) {
    int l=0;
    for(;i;i/=10) l++;
    return l==0 ? 1 : l;
}

void uint64_to_string( uint64 value, std::string& result ) {
    result.clear();
    result.reserve( 20 ); // max. 20 digits possible
    uint64 q = value;
    do {
        result += "0123456789"[ q % 10 ];
        q /= 10;
    } while ( q );
    std::reverse( result.begin(), result.end() );
}

// https://stackoverflow.com/questions/9695720/how-do-i-convert-a-64bit-integer-to-a-char-array-and-back
void int64ToChar(char mesg[], int64_t num) {
    *(int64_t *)mesg = htonl(num);
}

// https://stackoverflow.com/questions/24538954/how-do-you-cast-a-uint64-to-an-int64
uint64_t Int64_2_UInt64(int64_t value)
{
     return (((uint64_t)((uint32_t)((uint64_t)value >> 32))) << 32) 
        | (uint64_t)((uint32_t)((uint64_t)value & 0x0ffffffff));           
}

int64_t UInt64_2_Int64(uint64_t value)
{
    return (int64_t)((((int64_t)(uint32_t)((uint64_t)value >> 32)) << 32) 
       | (int64_t)((uint32_t)((uint64_t)value & 0x0ffffffff)));           
}

int32_t uint32_to_int32(uint32_t value)
{
	int32_t tmp;
	std::memcpy(&tmp, &value, sizeof(tmp));
	return value;//tmp;
}

int64_t uint64_to_int64(uint64_t value)
{
	int64_t tmp;
	std::memcpy(&tmp, &value, sizeof(tmp));
	return tmp;
}

char *myhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(int16_t *)dest = htobe16(*(int16_t *)src);
    break;
  case 4:
    *(int32_t *)dest = htobe32(*(int32_t *)src);
    break;
  case 8:
    *(int64_t *)dest = htobe64(*(int64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

char *umyhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(uint16_t *)dest = htobe16(*(uint16_t *)src);
    break;
  case 4:
    *(uint32_t *)dest = htobe32(*(uint32_t *)src);
    break;
  case 8:
    *(uint64_t *)dest = htobe64(*(uint64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

// https://www.techiedelight.com/get-slice-sub-vector-from-vector-cpp/
template<typename T>
std::vector<T> vec_slice(std::vector<T> const &v, int s, int e)
{
	auto first = v.cbegin() + s;
	auto last = v.cbegin() + e;
	std::vector<T> vec(first, last);
	return vec;
}

class koralROS
{
public:
	koralROS(FeatureDetector &detector_, FeatureMatcher &matcher_, std::string &s1topic, std::string &cameraTransport, std::string &color_encoding_, std::string &db_conn_str_, int &thread_count_, bool &display_features_, bool &morton_code_only_, std::string &target_table_name_, int &targetFeaturesCount_, bool &process_changed_pixels_only_, int &bgsubtractor_history_Length_, double &bgsubtractor_var_threshold_, bool &bgsubtractor_detect_shadows_, int &error_count_limit_, int &iterations_limit_) : detector(detector_), matcher(matcher_), thread_count(thread_count_), display_features(display_features_), morton_code_only(morton_code_only_), target_table_name(target_table_name_), targetFeaturesCount(targetFeaturesCount_), process_changed_pixels_only(process_changed_pixels_only_), bgsubtractor_history_Length(bgsubtractor_history_Length_), bgsubtractor_var_threshold(bgsubtractor_var_threshold_), bgsubtractor_detect_shadows(bgsubtractor_detect_shadows_), error_count_limit(error_count_limit_), iterations_limit(iterations_limit_)
	{
		ROS_DEBUG("thread_count: %i", thread_count);
		threads = std::vector<std::thread>(thread_count);
		pgbackend = std::make_shared<PGBackend>(db_conn_str_, thread_count);
		ros::master::V_TopicInfo topic_infos;
		ros::master::getTopics(topic_infos);
		for(int i, l = topic_infos.size(); i < l; i++) {
			if((topic_infos[i].name).find(s1topic) != std::string::npos)
			{
				type_hash = uint32_to_int32( CityHash32(topic_infos[i].datatype.c_str(), ((size_t)sizeof(topic_infos[i].datatype.size()))) );
			}
		}
		ROS_DEBUG("type_hash: %i", type_hash);

		if(color_encoding_.compare("BGR8") == 0)
		{
			color_encoding = sensor_msgs::image_encodings::BGR8;
		}
		else if(color_encoding_.compare("RGB8") == 0)
		{
			color_encoding = sensor_msgs::image_encodings::RGB8;
		}

		if(morton_code_only)
		{
			sql_copy_statement = "COPY " + target_table_name + "(v, t, i) FROM STDIN (FORMAT binary);";
		}
		else
		{
			sql_copy_statement = "COPY " + target_table_name + "(t, i, v1, v2, v3, v4, v5, v6, v7, v8, mc) FROM STDIN (FORMAT binary);";
		}
		bgsubtractor = cv::cuda::createBackgroundSubtractorMOG2(bgsubtractor_history_Length, bgsubtractor_var_threshold, bgsubtractor_detect_shadows);

//  <T> used here and associated below to replace ugly if else...?
		// std::cout << "cameraTransport: " << cameraTransport << std::endl;	
		if(cameraTransport.compare(COMPRESSED) == 0)
		{
			comp_image_sub_ptr.reset(new message_filters::Subscriber<sensor_msgs::CompressedImage>(node, s1topic, 1));
			comp_image_sub_ptr->registerCallback(boost::bind(&koralROS::compressedImageCallback, this, _1));
		} 
		else if(cameraTransport.compare(RAW) == 0)
		{
			image_sub_ptr.reset(new message_filters::Subscriber<sensor_msgs::Image>(node, s1topic, 1));
			image_sub_ptr->registerCallback(boost::bind(&koralROS::imageCallback, this, _1));
		}

	}

	~koralROS() {
		threads.clear();
		cv::destroyAllWindows();
		detector.freeGPUMemory();
		matcher.freeGPUMemory();
		cudaDeviceReset();
	}

	void compressedImageCallback(const sensor_msgs::CompressedImageConstPtr& img)	
	{
		cv_bridge::CvImagePtr imagePtr;
		try {
			imagePtr = cv_bridge::toCvCopy(img, color_encoding);
		}
		catch(cv_bridge::Exception& e) {
			ROS_ERROR("cv_bridge exception: %s", e.what());
			addErrorCount();
			return;
		}
		return imageCallback(imagePtr->toImageMsg());
	}

	void imageCallback(const sensor_msgs::ImageConstPtr& img)
	{
		ROS_DEBUG("---------------------------------");

		ts = high_resolution_clock::now();
		cv_bridge::CvImagePtr imagePtr;
		try {
			imagePtr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
		}
		catch(cv_bridge::Exception& e) {
			ROS_ERROR("cv_bridge exception: %s", e.what());
			addErrorCount();
			return;
		}
		high_resolution_clock::time_point t1 = high_resolution_clock::now();
		ROS_DEBUG("Convert to grayscale duration: %i microseconds", duration_cast<microseconds>( t1 - ts ).count());

		float featuresCount = (float)detector.kps.size();
		float percentageOfTarget = ((float)featuresCount/(float)targetFeaturesCount)*(float)100;
		ROS_DEBUG("featuresCount: %i targetFeaturesCount: %i percentageOfTarget: %.2f", (int)featuresCount, (int)targetFeaturesCount, percentageOfTarget);
		if(targetFeaturesCount > 0)
		{
			if(percentageOfTarget < 95)
			{
				if(fastThreshold >= 5)
				{
					fastThreshold -= 3; 
				}
			} 
			else if(percentageOfTarget > 105)
			{
				if(fastThreshold <= 95)
				{
					fastThreshold += 3; 
				}
			}
			if(percentageOfTarget < 95 || percentageOfTarget > 105)
			{
				ROS_DEBUG("setting FAST threshold: %i", fastThreshold);		
				detector.setFastThreshold(fastThreshold);
			}
		}

		equalizeHist( imagePtr->image, imagePtr->image );
		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		ROS_DEBUG("Equalise source image duration: %i microseconds", duration_cast<microseconds>( t2 - t1 ).count());

		if(process_changed_pixels_only)
		{
			ROS_DEBUG("Processing changed pixels only. bgsubtractor_history_Length: %i", bgsubtractor_history_Length);
			cv::cuda::GpuMat bgMask(imagePtr->image.size(), CV_8UC3, cv::Scalar::all(0)), srcImage, outImageBuf;
			srcImage = cv::cuda::GpuMat(imagePtr->image);
			bgsubtractor->setVarThreshold(bgsubtractor_history_Length);
			bgMask.setTo(0);
			bgsubtractor->apply(srcImage, bgMask, -1);
			srcImage.copyTo(outImageBuf, bgMask);
 			outImageBuf.download(imagePtr->image);
			bgMask.release();
			srcImage.release();
			outImageBuf.release();
			t2 = high_resolution_clock::now();
			ROS_DEBUG("Process changed pixels only duration: %i microseconds", duration_cast<microseconds>( t2 - t1 ).count());
		}

		detector.extractFeatures(imagePtr);
		t2 = high_resolution_clock::now();
		ROS_DEBUG("Extract features duration: %i microseconds", duration_cast<microseconds>( t2 - t1 ).count());

		if(display_features)
		{
			cv::drawKeypoints(imagePtr->image, detector.converted_kps, image_with_kps, cv::Scalar::all(-1.0), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
		}
		imagePtr->image.release();

		if(detector.desc.size() == 0)
		{
			detector.desc.clear();
			ROS_ERROR("No descriptors found");
			addErrorCount();
			return;
		}

		if(detector.desc.at(0) == 0)
		{
			detector.desc.clear();
			ROS_ERROR("Descriptor error - kernel returning 0");
			addErrorCount();
			return;
		}
	
		
		// spawn n threads:
		int desc_idx = 0;
		int chunk_size = (detector.kps.size() - ( detector.kps.size() % thread_count )) / thread_count;
		for (int i = 0; i < thread_count; i++) {
			desc_idx = i * 8 * chunk_size;
			std::vector<uint64_t> desc_chunk;
			std::vector<Keypoint> kps_chunk;
			int rem = 0;
			if(i == thread_count-1)
			{
				rem = detector.kps.size() % thread_count;
			}
			kps_chunk = vec_slice(detector.kps, i * chunk_size, (i * chunk_size) + chunk_size + rem);
			desc_chunk = vec_slice(detector.desc, desc_idx, (i+1) * 8 * chunk_size + (rem * 8));
			threads[i] = std::thread(boost::bind(&koralROS::persistDescriptors, this, _1, _2), desc_chunk, kps_chunk);
			// desc_chunk.clear();
			// kps_chunk.clear();
		}
		for (auto& th : threads) {
			th.join();
		}
		high_resolution_clock::time_point t3 = high_resolution_clock::now();
		ROS_DEBUG("Persist features duration: %i microseconds", duration_cast<microseconds>( t3 - t2 ).count());
		ROS_DEBUG("Pipeline total duration: %i microseconds", duration_cast<microseconds>( t3 - ts ).count());
		
		detector.receivedImg = true;

		if(iterations_limit > 0) 
		{
			ROS_DEBUG("iterations_limit: %i Current iteration: %i", iterations_limit, iterations_ctr+1);
			if(++iterations_ctr == iterations_limit){
				ROS_INFO("Iterations limit reached. Exiting...");
				exit(0);
			}
		}

	}

	cv::Mat image_with_kps;
	std::vector<cv::KeyPoint> kps;

	void persistDescriptors(std::vector<uint64_t> desc, std::vector<Keypoint> kps)
	{
		/* Binary COPY demo */
		char header[12] = "PGCOPY\n\377\r\n\0";
		int buf_pos = 0;
		int flag = 0;
		int extension = 0;
		int row_count = kps.size();
		int buffer_size = (((row_count) * 126)+21); // v1...v8+mc
		if(morton_code_only)
		{
			buffer_size = (((row_count) * 30)+21); // mc
		}
		// std::cout << "buffer size on create: " << std::to_string(buffer_size) << std::endl;
		char buffer[buffer_size];
		int size = 0;
		buf_pos += 11;
		// std::cout << "buf_pos 1: " << std::to_string(buf_pos) << std::endl;
		memcpy(buffer, header, buf_pos);		
		memcpy(&buffer[buf_pos], (void *)&flag, 4);
		buf_pos += 4;
		// std::cout << "buf_pos 2: " << std::to_string(buf_pos) << std::endl;
		memcpy(&buffer[buf_pos], (void *)&extension, 4);
		buf_pos += 4;
		// std::cout << "buf_pos 3: " << std::to_string(buf_pos) << std::endl;
		
		short fieldnum = 0;
		int32_t id = 0;
		int64_t mc = 0;
		int64_t val = 0;
		int oid_length = 4;
		int oid = 0;
		int desc_idx = 0;
		char buff[21];

		if(morton_code_only)
		{
			for(int i = 0, l = row_count; i < l; i++) 
			{
				desc_idx = i * 8;

				fieldnum = 3; // v (mc), t, i
				memcpy(&buffer[buf_pos], myhton((char *)&fieldnum, 2), 2);
				buf_pos += 2;

				size = 8;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				mc = uint64_to_int64(MortonND_8D::Encode( desc.at(desc_idx+7), desc.at(desc_idx+6), desc.at(desc_idx+5), desc.at(desc_idx+4), desc.at(desc_idx+3), desc.at(desc_idx+2), desc.at(desc_idx+1), desc.at(desc_idx) ) );
				memcpy(&buffer[buf_pos], myhton((char *)&mc, 8), 8);
				buf_pos += 8;

				// type
				size = 4;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;

				memcpy(&buffer[buf_pos], &type_hash, 4);
				buf_pos += 4;

				// id: hash of x + y
				size = 4;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;

				id = Pack16_to_32(kps.at(i).x, kps.at(i).y);
				// std::cout << "ID x: " << std::to_string(kps.at(i).x) << " ID y: " << std::to_string(kps.at(i).y) << " ID: " << std::to_string(id) <<  std::endl;
				memcpy(&buffer[buf_pos], myhton((char *)&id, 4), 4);
				buf_pos += 4;

			}
		}
		else
		{
			for(int i = 0, l = row_count; i < l; i++) 
			{
				desc_idx = i * 8;
				fieldnum = 11; // v1...v8+mc

				memcpy(&buffer[buf_pos], myhton((char *)&fieldnum, 2), 2);
				buf_pos += 2;
				// std::cout << "buf_pos 4: " << std::to_string(buf_pos) << std::endl;

				// type
				size = 4;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;

				memcpy(&buffer[buf_pos], &type_hash, 4);
				buf_pos += 4;

				// id: hash of x + y
				size = 4;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 5: " << std::to_string(buf_pos) << std::endl;
				id = Pack16_to_32(kps.at(i).x, kps.at(i).y);
				// std::cout << "ID x: " << std::to_string(kps.at(i).x) << " ID y: " << std::to_string(kps.at(i).y) << " ID: " << std::to_string(id) <<  std::endl;
				memcpy(&buffer[buf_pos], myhton((char *)&id, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 6: " << std::to_string(buf_pos) << std::endl;

				// Start values v1, v2...
				size = 8;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
				val = uint64_to_int64(desc.at(desc_idx));
				memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
				buf_pos += 8;
				// std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

				size = 8;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
				val = uint64_to_int64(desc.at(desc_idx+1));
				memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
				buf_pos += 8;
				// std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

				size = 8;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
				val = uint64_to_int64(desc.at(desc_idx+2));
				memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
				buf_pos += 8;
				// std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

				size = 8;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
				val = uint64_to_int64(desc.at(desc_idx+3));
				memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
				buf_pos += 8;
				// std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

				size = 8;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
				val = uint64_to_int64(desc.at(desc_idx+4));
				memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
				buf_pos += 8;
				// std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

				size = 8;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
				val = uint64_to_int64(desc.at(desc_idx+5));
				memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
				buf_pos += 8;
				// std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

				size = 8;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
				val = uint64_to_int64(desc.at(desc_idx+6));
				memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
				buf_pos += 8;
				// std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;
				
				size = 8;
				memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
				buf_pos += 4;
				// std::cout << "buf_pos 7: " << std::to_string(buf_pos) << std::endl;
				val = uint64_to_int64(desc.at(desc_idx+7));			
				memcpy(&buffer[buf_pos], myhton((char *)&val, 8), 8);
				buf_pos += 8;
				// std::cout << "buf_pos 8: " << std::to_string(buf_pos) << std::endl;

			}
		}

		// end of file
		short negative = -1;
		memcpy(&buffer[buf_pos], myhton((char *)&negative, 2), 2);
		
		// high_resolution_clock::time_point t2 = high_resolution_clock::now();
   		// auto tuple_duration = duration_cast<milliseconds>( t2 - t1 ).count();
		// PGconn *conn = pgbackend->connection()->connection().get();
		auto conn = pgbackend->connection();
		PGresult *res = NULL;
		res = PQexec(conn->connection().get(), sql_copy_statement.c_str());
		if (PQresultStatus(res) != PGRES_COPY_IN)
		{
			ROS_ERROR("Postgres not in COPY_IN mode");
			PQclear(res);
			addErrorCount();
		}
		else
		{
			PQclear(res);
			int copyRes = PQputCopyData(conn->connection().get(), buffer, buffer_size);
			if (copyRes == 1)
			{
				if (PQputCopyEnd(conn->connection().get(), NULL) == 1)
				{
					res = PQgetResult(conn->connection().get());
					if (PQresultStatus(res) != PGRES_COMMAND_OK)
					{
						ROS_DEBUG("Postgres command success: %s", PQerrorMessage(conn->connection().get()));
					}
					PQclear(res);
				}
				else
				{
					ROS_ERROR("Postgres connection error: %s", PQerrorMessage(conn->connection().get()));
					addErrorCount();
				}
			}
			else if (copyRes == 0)
			{
				ROS_WARN("No data sent, connection is in nonblocking mode");
			}
			else if (copyRes == -1)
			{
				ROS_ERROR("Postgres error writing data: %s", PQerrorMessage(conn->connection().get()));
				addErrorCount();
			}
		}
		pgbackend->freeConnection(conn);
		// delete [] buffer;
		// delete [] buff;
		// delete [] header;
	}

private:

	void addErrorCount(){
		if(error_count_limit > -1) 
		{
			if(++error_count == error_count_limit){
				ROS_INFO("Error count limit reached. Exiting...");
				ros::shutdown();
				exit(0);
			}
		}
	}

	int iterations_limit = -1;
	int iterations_ctr = 0;
	int error_count_limit = -1;
	int error_count = 0;

	std::vector<std::thread> threads;
	int thread_count = 1;

	int32_t type_hash = 0;

	std::shared_ptr<PGBackend> pgbackend;

	const std::string RAW = "raw";
	const std::string COMPRESSED = "compressed";

	std::string color_encoding = sensor_msgs::image_encodings::BGR8;

	ros::NodeHandle node;
	FeatureDetector &detector;
	FeatureMatcher &matcher;

	boost::shared_ptr<message_filters::Subscriber<sensor_msgs::CompressedImage>> comp_image_sub_ptr;
	boost::shared_ptr<message_filters::Subscriber<sensor_msgs::Image>> image_sub_ptr;

	high_resolution_clock::time_point ts;
	high_resolution_clock::time_point te;

	bool display_features = false;

	bool morton_code_only = false;

	bool process_changed_pixels_only = false;

	std::string target_table_name = "input_stream";
	std::string sql_copy_statement = "";

	uint8_t fastThreshold = 50;
	float targetFeaturesCount = 0;

	int bgsubtractor_history_Length = 500;
	double bgsubtractor_var_threshold = 16;
	bool bgsubtractor_detect_shadows = false;
	cv::Ptr<cv::cuda::BackgroundSubtractorMOG2> bgsubtractor;	

};

int main(int argc, char **argv)
{
	int thread_count = 1;
	int iterations_limit = 10;
	int error_count_limit = -1;
	int opt = 0;
	unsigned int width = 640;
	unsigned int height = 480;
	unsigned int maxFeatureCount = 5000;
	uint8_t fastThreshold = 50;
	int targetFeaturesCount = 0;
	uint8_t matchThreshold = 25;
	unsigned int scaleLevels = 8;
	float scaleFactor = 1.2;
	
	std::string target_table_name = "";

	std::string topicName = ""; 
	std::string cameraTransport = "compressed"; // unused currently
	std::string color_encoding = "BGR8";

	std::string postgresHostIP = "";
	std::string postgresPort = "6432";
	std::string postgresDBName = "postgres";
	std::string postgresUser = "postgres";
	std::string postgresPassword = "password";
	std::string postgresSchemas = "public";

	bool displayFeatures = false;
	bool morton_code_only = false;
	bool process_changed_pixels_only = false;	

	int bgsubtractor_history_Length = 500;
	double bgsubtractor_var_threshold = 16; 
	bool bgsubtractor_detect_shadows = false;

	std::string container_id = "";

	const char *opts = "+"; // set "posixly-correct" mode
	const option longopts[]{
		{"width", 1, 0, '0'},
		{"height", 1, 0, '1'},
		{"maxFeatureCount", 1, 0, '2'},
		{"fastThreshold", 1, 0, '3'},
		{"matchThreshold", 1, 0, '4'},
		{"scaleLevels", 1, 0, '5'},
		{"scaleFactor", 1, 0, '6'},
		{"topicName", 1, 0, '7'},
		{"cameraTransport", 1, 0, '8'},
		{"postgresHostIP", 1, 0, '9'},
		{"postgresPort", 1, 0, 'a'},
		{"postgresDBName", 1, 0, 'b'},
		{"postgresUser", 1, 0, 'c'},
		{"postgresPassword", 1, 0, 'd'},
		{"displayFeatures", 1, 0, 'e'},
		{"containerId", 1, 0, 'f'},
		{"thread_count", 1, 0, 'g'},
		{"color_encoding", 1, 0, 'h'},
		{"morton_code_only", 1, 0, 'i'},
		{"target_table_name", 1, 0, 'j'},
		{"postgresSchemas", 1, 0, 'k'},
		{"target_features_count", 1, 0, 'l'},
		{"process_changed_pixels_only", 1, 0, 'm'},
		{"bgsubtractor_history_Length", 1, 0, 'n'},
		{"bgsubtractor_var_threshold", 1, 0, 'o'},
		{"bgsubtractor_detect_shadows", 1, 0, 'p'},
		{"iterations_limit", 1, 0, 'q'},
		{"error_count_limit", 1, 0, 'r'},
		{0, 0, 0, 0}}; 
	
	ROS_INFO("Processing parameters...");
	while ((opt = getopt_long_only(argc, argv, opts, longopts, 0)) != -1)
	{
		switch (opt)
		{
		case '3':
			fastThreshold = atoi(optarg);
			ROS_INFO("setting fastThreshold: %i", fastThreshold);
			break;
		case '4':
			matchThreshold = atoi(optarg);
			ROS_INFO("setting matchThreshold: %i", matchThreshold);
			break;
		case '2':
			maxFeatureCount = strtoul(optarg, NULL, 0);
			ROS_INFO("setting maxFeatureCount: %d", maxFeatureCount);
			break;
		case '5':
			scaleLevels = atoi(optarg);
			ROS_INFO("setting scaleLevels: %i", scaleLevels);
			break;
		case '6':
			scaleFactor = atof(optarg);
			ROS_INFO("setting scaleFactor: %f", scaleFactor);
			break;
		case '0':
			width = atoi(optarg);
			ROS_INFO("setting width: %i", width);
			break;
		case '1':
			height = atoi(optarg);
			ROS_INFO("setting height: %i", height);
			break;
		case '7':
			topicName = optarg;
			ROS_INFO("setting topicName: %s", topicName.c_str());
			break;
		case '8':
			cameraTransport = optarg;
			ROS_INFO("setting cameraTransport: %s", cameraTransport.c_str());
			break;
		case '9':
			postgresHostIP = optarg;
			ROS_INFO("setting postgresHostIP: %s", postgresHostIP.c_str());
			break;
		case 'a':
			postgresPort = optarg;
			ROS_INFO("setting postgresPort: %s", postgresPort.c_str());
			break;
		case 'b':
			postgresDBName = optarg;
			ROS_INFO("setting postgresDBName: %s", postgresDBName.c_str());
			break;
		case 'c':
			postgresUser = optarg;
			ROS_INFO("setting postgresUser: %s", postgresDBName.c_str());
			break;
		case 'd':
			postgresPassword = optarg;
			ROS_INFO("setting postgresPassword: %s", postgresPassword.c_str());
			break;
		case 'e':
			displayFeatures = !(std::string(optarg).compare("true")) ? true : false;
			ROS_INFO("setting displayFeatures: %s", displayFeatures ? "true" : "false");
			break;
		case 'f':
			container_id = optarg;
			ROS_INFO("setting container_id: %s", container_id.c_str());
			break;
		case 'g':
			thread_count = atoi(optarg);
			ROS_INFO("setting thread_count: %i", thread_count);
			break;
		case 'h':
			color_encoding = optarg; // update to enum
			ROS_INFO("setting color_encoding: %s", color_encoding.c_str());
			break;
		case 'i':
			morton_code_only = !(std::string(optarg).compare("true")) ? true : false;
			ROS_INFO("setting morton_code_only: %s", morton_code_only ? "true" : "false");
			break;
		case 'j':
			target_table_name = optarg;
			ROS_INFO("setting target_table_name: %s", target_table_name.c_str());
			break;
		case 'k':
			postgresSchemas = optarg;
			ROS_INFO("setting postgresSchemas: %s", postgresSchemas.c_str());
			break;			
		case 'l':
			targetFeaturesCount = atoi(optarg);
			ROS_INFO("setting targetFeaturesCount: %i", targetFeaturesCount);
			break;
		case 'm':
			process_changed_pixels_only = !(std::string(optarg).compare("true")) ? true : false;
			ROS_INFO("setting process_changed_pixels_only: %s", process_changed_pixels_only ? "true" : "false");
			break;
		case 'n':
			bgsubtractor_history_Length = atoi(optarg);
			ROS_INFO("setting bgsubtractor_history_Length: %i", bgsubtractor_history_Length);
			break;
		case 'o':
			bgsubtractor_var_threshold = atof(optarg);
			ROS_INFO("setting bgsubtractor_var_threshold: %f", bgsubtractor_var_threshold);
			break;
		case 'p':
			bgsubtractor_detect_shadows = !(std::string(optarg).compare("true")) ? true : false;
			ROS_INFO("setting bgsubtractor_var_threshold: %s", bgsubtractor_detect_shadows ? "true" : "false");
			break;
		case 'q':
			iterations_limit = atoi(optarg);
			ROS_INFO("setting iterations_limit: %i", iterations_limit);
			break;
		case 'r':
			error_count_limit = atoi(optarg);
			ROS_INFO("setting error_count_limit: %i", error_count_limit);
			break;
		default: 
			exit(EXIT_FAILURE);
		}
	}

	ROS_INFO("Starting...");

	std::string ros_node_name = "koralROS_" + container_id;

	ros::init(argc, argv, ros_node_name);
	ros::NodeHandle nh;

	ros::master::V_TopicInfo topic_infos;
  	ros::master::getTopics(topic_infos);
	ROS_INFO("ROS topics: ");
	for(int i, l = topic_infos.size(); i < l; i++) {
		ROS_INFO("topic name: %s \n\t\t\t\ttopic type: %s", topic_infos[i].name.c_str(), topic_infos[i].datatype.c_str());
	}
	// defaults to Unix socket - 30% faster than TCP
	std::string db_conn_str = "dbname='" + postgresDBName + "' user='" + postgresUser + "' password='" + postgresPassword + "' options='-c search_path=" + postgresSchemas + "'";
	// only if host IP specified then switch to TCP
	if(postgresHostIP.length() > 0)
	{
		db_conn_str = "postgresql://" + postgresUser + ":" + postgresPassword + "@" + postgresHostIP + ":" + postgresPort + "/" + postgresDBName + "?options=-c%20search_path%3D" + postgresSchemas;
	}
	ROS_INFO("Database connection string: %s", db_conn_str.c_str());
	char conninfo[db_conn_str.size() + 1];
	strcpy(conninfo, db_conn_str.c_str());
	
	FeatureDetector detector(scaleFactor, scaleLevels, width, height, maxFeatureCount, fastThreshold);
	FeatureMatcher matcher(matchThreshold, maxFeatureCount);

	koralROS koral(detector, matcher, topicName, cameraTransport, color_encoding, db_conn_str, thread_count, displayFeatures, morton_code_only, target_table_name, targetFeaturesCount, process_changed_pixels_only, bgsubtractor_history_Length, bgsubtractor_var_threshold, bgsubtractor_detect_shadows, error_count_limit, iterations_limit);
	
	if(displayFeatures)
	{
		while (ros::ok())
		{
			ros::spinOnce();
			if (detector.receivedImg)
			{
				detector.converted_kps.clear();
				cv::namedWindow("Keypoints-"+container_id, cv::WINDOW_NORMAL | cv::WINDOW_KEEPRATIO);
				cv::resizeWindow("Keypoints-"+container_id, width, height);
				cv::imshow("Keypoints-"+container_id, koral.image_with_kps);
				cv::waitKey(1);
				detector.receivedImg = false;
			}
		}			
	}
	else
	{
		ros::spin();
	}
	ros::shutdown();
	return 0;
}
