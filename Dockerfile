FROM millertech/ros-opencv-cuda-python3:latest
LABEL maintainer dammian.miller@gmail.com

WORKDIR /opt

# GCC7
RUN apt-get install -y software-properties-common \
    && add-apt-repository ppa:ubuntu-toolchain-r/test \
    && apt update \
    && apt install g++-7 -y \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 60 \
    --slave /usr/bin/g++ g++ /usr/bin/g++-7 \
    && update-alternatives --config gcc

# VCPKG
RUN apt   install -y curl zip unzip tar \
    && git clone https://github.com/Microsoft/vcpkg.git \
    && cd vcpkg \
    && ./bootstrap-vcpkg.sh \
    && ./vcpkg integrate install \
    && ln -s /opt/vcpkg/vcpkg /usr/bin/vcpkg

# morton-ndXXHASH
RUN vcpkg install morton-nd

# PostgreSQL 12 Headers
RUN apt update && apt install -y postgresql-common \
    && yes | sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh \
    && apt install -y --allow-unauthenticated libpqxx-dev libpq-dev postgresql-server-dev-all

# CMAKE 3.17
RUN wget https://github.com/Kitware/CMake/releases/download/v3.17.0/cmake-3.17.0.tar.gz \
    && tar xvfz cmake-3.17.0.tar.gz \
    && cd cmake-3.17.0 \
    && ./configure \
    && make -j$(nproc) \
    && make install

ENV CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH}:/opt/vcpkg/installed/x64-linux/share
# cityhash
RUN apt-get install -y automake autoconf && git clone https://github.com/google/cityhash.git \
&& cd cityhash/ \
&& ./configure --enable-sse4.2 \
&& make -j$(nproc) all check CXXFLAGS="-g -O3 -msse4.2" \
&& make install

COPY cmake/cityhashConfig.cmake /usr/local/share/cmake-3.17/Modules/Findcityhash.cmake
# ENV CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}:/usr/local/share/cmake-3.17/Modules

# KORAL-ROS-POSTGRES
RUN . ~/ros_catkin_ws/install_isolated/setup.sh  \
    && git clone --recurse-submodules https://bitbucket.org/dammian_miller/koral-ros-postgres.git \
    && cd koral-ros-postgres \
    && mkdir build  &&  cd  build \
    && cmake  .. \
    && make -j$(nproc)

# RUN apt install -y ros-noetic-image-view
# RUN wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add - \
# && apt update \
# && apt-get install -y ros-noetic-video-stream-opencv --no-install-recommends

# enables docker-slim to run
RUN apt update && apt install -y python3.8-venv; \
python3 -m venv /opt/venv; \
. /opt/venv/bin/activate

RUN cd /usr/lib/x86_64-linux-gnu; \
cp /usr/local/lib/libopencv_imgcodecs.so.4.8.0 .
RUN cd /usr/lib/x86_64-linux-gnu; \
rm libopencv_imgcodecs.so
RUN cd /usr/lib/x86_64-linux-gnu; \
ln -s libopencv_imgcodecs.so.4.8.0 libopencv_imgcodecs.so

WORKDIR /opt/koral-ros-postgres
SHELL ["/bin/bash", "-c"]
CMD ["/bin/bash", "-c", "./run.sh"]
