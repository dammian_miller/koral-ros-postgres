--------------------
# KORAL_ROS_POSTGRES
--------------------
This is a ROS to Postgres visual feature pipelines based on the (excellent) KORAL_ROS codebase, extended with the following capabilities:

- compressed and raw video/image support
- rgb8 and bgr8 colorspace support
- CUDA 10.2/Turing support
- parameterized runtime configuration
- Docker container description with environment variables mapped to runtime parameters
- Postgres/TimescaleDB integration for stream processing, data storage
- Postgres connection pool
- Morton code generation (indexing, NN lookups, etc)
- descriptor coordinate as ID for analytics
- mult-threading descriptor processing, write to DB

A Dockerfile is provided for both the runtime and dev environments.  Configuration of the Dockerfile is via environment parameters.  Availalbe parameters are:

- WIDTH - this must match the video width of the ROS topic
- HEIGHT - this must match the video height of the ROS topic
- MAX_FEATURE_COUNT - this determines the memory allocated on the GPU for features.  Too small and the CUDA kernel errors
- FAST_THRESHOLD - the threshold for the FAST keypoint detector to identify keypoints
- SCALE_LEVELS - adjust number of CLATCH scale levels
- SCALE_FACTOR - adjust CLATCH scale factor
- TOPIC_NAME - the ROS tropic name as displayed by the ROS command rostopic list eg. /camera/image_raw/compressed
- CONTAINER_ID - unique container ID to enable multiple visual pipelines within the same ROS Master
- THREAD_COUNT - number of threads/DB connections.  There is a 1:1 mapping of threads to DB connections.
- POSTGRES_HOST_IP - postgres parameters are self-describing
- POSTGRES_PORT
- POSTGRES_USER
- POSTGRES_PASSWORD
- POSTGRES_DB_NAME
- DISPLAY_FEATURES - display an opencv window showing the video feed with feature keypoints overlaid
- CAMERA_TRANSPORT - compressed or raw
- COLOR_ENCODING - rgb8 or bgr8
- TARGET_FEATURES_COUNT - dynamically adjusts the FAST threshold to identify a target number of features to extract per frame
- PROCESS_CHANGED_PIXELS_ONLY - detect and extract features from pixels that have changed

An example Docker run command:

docker run \

	--gpus '"device=0"' \ #pass through raw GPU device
	--net host \
	-it \
	--privileged \
	-e DISPLAY_FEATURES=true \ #display opencv windows with features overlaid on video stream
	-e ROS_MASTER_URI=http://127.0.0.1:11311 \ #ROS URI
	-e MAX_FEATURE_COUNT=131072 \ #allocates mem for features on GPU.  Too few=segfault - right size for your workload
	-e SCALE_LEVELS=8 \ # CLATCH Scale Levels
	-e FAST_THRESHOLD=40 \
	-e POSTGRES_USER=postgres \
	-e CONTAINER_ID=1 \ # enables multiple ROS nodes with different names
	-e TOPIC_NAME=/camera/image_raw/compressed \ # ROS topic name <- available topics are printed on first run of the pipeline
	-e DISPLAY=$DISPLAY \ # pass the X11 display
	-v ~/postgresql/run/.s.PGSQL.5432:/var/run/postgresql/.s.PGSQL.5432 \ #passing the unix socket for the DB connection - this is 30% faster than TCP.  TCP can be used if required by mapping ports eg. -p 5432:5432
	-v /tmp/.X11-unix:/tmp/.X11-unix \ 
	-v ${HOME}/dev/pipelines/visual/koral-ros-postgres/:/koral-ros-postgres \ # mapping local dir this repo is checked out to for dev use
	/bin/bash #optional - use only if using the container for dev, otherwise omit this and the container autoruns based on environment parameters provided

Note that a TimescaleDB/Postgres server running on the mapped Unix socket or a TCP socker using the -p 5432:<host port> is required for operation.  A Unix socket was used in this example as it is roughly 30% faster than TCP.

Also note the dev repo volume mapping:

${HOME}/dev/pipelines/visual/koral-ros-postgres/:/koral-ros-postgres

This assumes you checkout this repo to:

${HOME}/dev/pipelines/visual/koral-ros-postgres

Using your IDE/editor edit the files from the host at:

${HOME}/dev/pipelines/visual/koral-ros-postgres

and compile and run from within the container at:

/koral-ros-postgres

To build, from the container shell, run:
  
  
cd /koral-ros-postgres

mkdir -p build

cd build

cmake ..

make -j$(nproc)  

  

Then, to run the pipeline, set appropriate environment params and use the dev_run.sh script:  
  
FAST_THRESHOLD=20 THREAD_COUNT=4 MAX_FEATURE_COUNT=250000 WIDTH=1280 HEIGHT=720 CAMERA_TRANSPORT=compressed TOPIC_NAME=/apollo/sensor/camera/traffic/image_short/compressed ../dev_run.sh  
  
Once dev is committed and PR accepted, re-build the Docker container to see the latest code in action by default.  

Note that the non-dev runtime is located at /opt/koral-ros-postgres within the container.  This is build from the main repo and auto-runs on startup if no override parameter e.g. /bin/bash (as above) is provided  

The Postgres schema expected by the pipeline is:

-- complete

create table if not exists m0
(

	ts timestamptz default now() not null,
	v1 bigint not null,
	v2 bigint not null,
	v3 bigint not null,
	v4 bigint not null,
	v5 bigint not null,
	v6 bigint not null,
	v7 bigint not null,
	v8 bigint not null,
	mc bigint not null,
	t integer not null,
	i integer not null
);

-- Morton code only

create table if not exists m0
(

	ts timestamptz default now() not null,
	v bigint not null,
	t integer not null,
	i integer not null
);


And now, the original KORAL_ROS repo docs...

----------------
# KORAL_ROS
----------------

KORAL_ROS is a ROS based computer vision pipeline, which combines GPU based feature detection, description and matching. 

**Main features:**

* [KFAST](https://github.com/komrad36/KFAST): FAST(!!) corner detection using AVX2 instructions
* [CLATCH](https://github.com/komrad36/CLATCH): CUDA based LATCH description for features, produces 512-bit binary descriptors. 
* [CUDAK2NN](https://github.com/komrad36/CUDAK2NN): Extremely fast Hamming distance based brute force matching.

This repository serves as an extension to [KORAL](https://github.com/komrad36/KORAL), an extremely fast, highly accurate, 
scale- and rotation-invariant CPU-GPU cooperative detector-descriptor that uses FAST
keypoints and LATCH descriptors, combining it with [CUDAK2NN](https://github.com/komrad36/CUDAK2NN), a super-fast GPU implementation 
of a brute-force matcher for 512-bit binary descriptors, both originally developed by 
[Kareem Omar](https://github.com/komrad36). All credits for these amazingly fast kernels go to the original author(s).

In this repository, KORAL and CUDAK2NN have been adapted into a real time framework, where images are read
in succession on which detection and matching is performed. The sample code in koral_node.cpp subscribes to two ROS topics
'imageL' and 'imageR', simulating left and right camera views, performs feature extraction on 
both images and brute force matching between the two views. This is aimed at being a starting
point for GPU based vision algorithms for autonomous vehicles: considering that feature detection and matching
are some of the biggest bottlenecks, fast GPU based implementations can allow for real-time localization.

**Dependencies:**

* AVX2 capable CPU
* CUDA capable GPU
* ROS
* OpenCV (for retrieving keypoints and matches)

This code is meant only as an example to get started with, as numerous improvements can be 
made to the current functionality (example: asynchronous detection for multiple images). 

**Sample benchmark:** (i7-6770HQ, GTX 1080)

| Image resolution        | Detection (ms per image)           | Matching (ms per pair)  |
| :-------------: |:-------------:| :-----:|
| 640x480      | 3 | 1 |
| 1920x1080      | 7      |   4 |

Note: Matching performance depends on the number of total feature points that need to be evaluated.

**Parameters:** 

Feature extraction:

`FeatureDetector detector(scaleFactor, scaleLevels, width, height, maxFeatureCount, fastThreshold);`

* `scaleFactor`: Coefficient by which one scale pyramid level is divided to obtain the next.
* `scaleLevels`: Number of levels in the scale space. 
* `fastThreshold`: Threshold intensity difference between pixels (for classifying as a corner).

Feature matching:

`FeatureMatcher matcher(matchThreshold, maxFeatureCount);`

* `matchThreshold`: A match is returned if the best match between a query vector and a training vector is more than `matchThreshold` number of bits better than the second-best match.

[1]

# Licenses #
**KORAL is licensed under the MIT License : https://opensource.org/licenses/mit-license.php**

**Copyright(c) 2016 Kareem Omar, Christopher Parker**

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and / or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions :

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Note again that KORAL is a work in progress.
Suggestions and improvements are welcomed.

- - - -

The FAST detector was created by Edward Rosten and Tom Drummond
as described in the 2006 paper by Rosten and Drummond:
"Machine learning for high-speed corner detection"
        Edward Rosten and Tom Drummond
https://www.edwardrosten.com/work/rosten_2006_machine.pdf

**The FAST detector is BSD licensed:**

**Copyright(c) 2006, 2008, 2009, 2010 Edward Rosten**
**All rights reserved.**

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met :


*Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

*Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and / or other materials provided with the distribution.

*Neither the name of the University of Cambridge nor the names of
its contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT LIMITED TO,
	PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
	PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING
		NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


- - - -
